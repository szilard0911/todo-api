// const MongoClient = require('mongodb').MongoClient
const {MongoClient, ObjectID} = require('mongodb') 

MongoClient.connect('mongodb://localhost:27017/Todoapp', (err, db) => {
    if(err){
        return console.log('Unable to connect to MongoDB server')
    }
    console.log('Connected to MongoDB server')

    // db.collection('Todos').findOneAndUpdate({
    //     _id: new ObjectID('5988a80770be7e93947466bd')
    // }, {
    //     $set: {
    //         completed: true
    //     }
    // }, {
    //     returnOriginal: false
    // }).then((result) => {
    //     console.log(result)
    // })

    db.collection('Users').findOneAndUpdate({
        _id: new ObjectID('5988ab8870be7e9394746736')
    }, {
        $set: {
            name: 'Szilárd Papp',
        },
        $inc:{
            age: 1
        }
    }, {
        returnOriginal: false
    }).then((result) => {
        console.log(result)
    })

    // db.close()
})