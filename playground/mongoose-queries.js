const {ObjectID} = require('mongodb')

const {mongoose} = require('./../server/db/mongoose')
const {Todo} = require('./../server/models/todo')
const {User} = require('./../server/models/user')

var id = '6988e202da50cd0eb4c35beb'
var userId = '5988ba92e972880a90a3590d'

// if (!ObjectID.isValid(id)) {
//     console.log('ID not valid')
// }

// Todo.find({
//     _id: id
// }).then((todos) => {
//     console.log('todos: ', todos)
// })

// Todo.findOne({
//     _id: id
// }).then((todo) => {
//     console.log('todo: ', todo)
// })

// Todo.findById(id).then((todo) => {
//     if(!todo){
//         return console.log('Id not found')
//     }
//     console.log('todo by id: ', todo)
// }).catch((e) => {
//     console.log(e)
// })

User.findById(userId).then((user) => {
    if(!user){
        return console.log('User not found')
    }
    console.log(JSON.stringify(user, undefined, 2))
}).catch((e) => {
    console.log(e)
})
