// const MongoClient = require('mongodb').MongoClient

const {MongoClient, ObjectID} = require('mongodb') 

/**
 * CONNECT TO MONGODB
 * 
 * mongodb:// <- domain always this
 * url: localhost:27017
 * db: TodoApp
 * 
 * err: error object
 * db: requested db
 */
MongoClient.connect('mongodb://localhost:27017/Todoapp', (err, db) => {
    if(err){
        return console.log('Unable to connect to MongoDB server')
    }
    console.log('Connected to MongoDB server')

    // select * form Todos where (completed = false) _id = '5984e3d1cfd69720a8a04d7f'
    // db.collection('Todos').find({
    //     _id: new ObjectID('598878974cd4412e36a0d6a2')
    // }).toArray().then((docs) => {
    //     console.log('Todos')
    //     console.log(JSON.stringify(docs, undefined, 2))
    // }, (err) => {
    //     console.log('Unabele to fetch todos')
    // })

    // count // .then() <- promisse insted of passing data
    // db.collection('Todos').find().count().then((count) => {
    //     console.log(`Todos count: ${count}`)
    // }, (err) => {
    //     console.log('Unabele to fetch todos')
    // })

    db.collection('Users').find({name: 'Szilárd Papp'}).toArray().then((docs) => {
        console.log('Users')
        console.log(JSON.stringify(docs, undefined, 2))
    }, (err) => {
        console.log('Unabele to fetch todos')
    })
 
    // db.close()
})