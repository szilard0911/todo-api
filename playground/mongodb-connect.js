// const MongoClient = require('mongodb').MongoClient

// var user = {
//     name: 'Szilárd',
//     age: 29
// }
// // object destrcturing in ES6
// var {name} = user
// console.log(name)

const {MongoClient, ObjectID} = require('mongodb') //see the 1st row: with object destructing we can create two or more constans in one time
// generate a unique ObjectID, just for fun
var obj = new ObjectID()
console.log(obj)

/**
 * CONNECT TO MONGODB
 * 
 * mongodb:// <- domain always this
 * url: localhost:27017
 * db: TodoApp
 * 
 * err: error object
 * db: requested db
 */
MongoClient.connect('mongodb://localhost:27017/Todoapp', (err, db) => {
    if(err){
        return console.log('Unable to connect to MongoDB server')
    }
    console.log('Connected to MongoDB server')

    //mongo db creates the databaese at the first insert
    // db.collection('Todos').insertOne({
    //     text: 'something to do',
    //     completed: false
    // }, (err, result) => {
    //     if(err) {
    //         return console.log('Unable to insert todo', err)
    //     }

    //     console.log(JSON.stringify(result.ops, undefined, 2))
    // })


    
    // db.collection('Users').insertOne({
    //     name: 'Szilárd Papp',
    //     age: 29,
    //     location: 'Budapest'
    // }, (err, result) => {
    //     if (err) {
    //         return console.log('Unable to insert User')
    //     }
    //     console.log(JSON.stringify(result.ops[0]._id.getTimestamp(), undefined, 2))
    // })

    db.close()
})