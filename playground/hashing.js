const {SHA256} = require('crypto-js')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

var password = '123abc!'
// (numer of rounds, callback)
bcrypt.genSalt(10, (error, salt) => {
    bcrypt.hash(password, salt, (err, hash) => {
        console.log(hash)
    })
})
var hashedPassword = '$2a$10$.7vmxzjSB8Xr45U2s2aYU.SltU6kSPYgPZc88DIorWFD5l4ZjIlRu'

bcrypt.compare(password, hashedPassword, (err, res) => {
    console.log(res)
})

// var data = {
//     id: 10
// }

// var token = jwt.sign(data, '123abc')
// console.log(token)

// var decoded = jwt.verify(token, '123abc')
// console.log(decoded)

// var message = 'az édes faszom mindenbe'

// var hash = SHA256(message).toString()

// console.log(`Message ${message}`)
// console.log(`Hash ${hash}`)

// var data = {
//     id: 4
// }

// var token = {
//     data,
//     hash: SHA256(JSON.stringify(data) + 'somesecret').toString()
// }

// token.data = 5
// token.hash = SHA256(JSON.stringify(data)).toString()


// var resultHash = SHA256(JSON.stringify(token.data)+ 'somesecret').toString()

// if(resultHash === token.hash){
//     console.log('data was not changed')
// }{
//     console.log('data was changed, Do not trust')
// }