// const MongoClient = require('mongodb').MongoClient

const {MongoClient, ObjectID} = require('mongodb') 

MongoClient.connect('mongodb://localhost:27017/Todoapp', (err, db) => {
    if(err){
        return console.log('Unable to connect to MongoDB server')
    }
    console.log('Connected to MongoDB server')

    // deleteMany
    // db.collection('Todos').deleteMany({text: 'eat lunch'}).then((result) => {
    //     console.log(result)
    // })

    // deleteOne //delete the first
    // db.collection('Todos').deleteOne({text: 'eat lunch'}).then((result) => {
    //     console.log(result)
    // })

    // findOneAndDelete
    // db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
    //     console.log(result)
    // })


    // db.collection('Users').deleteOne({name: 'Szilárd Papp'}).then((result) => {
    //     console.log(result)
    // })

    // db.collection('Users').deleteMany({name: 'Szilárd Papp'}).then((result) => {
    //     console.log(result)
    // })

    db.collection('Users').findOneAndDelete({_id: new ObjectID('598873a67c21850b80b84617')}).then((result) => {
        console.log(JSON.stringify(result, undefined, 2))
    })


    // db.close()
})