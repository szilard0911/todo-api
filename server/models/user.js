const validator = require('validator')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const _ = require('lodash')
const bcrypt = require('bcryptjs')

var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        minlength: 1,
        trim: true,
        unique: true,
        validate: {
            /*validator: (value) => {
                return validator.isEmail(value)
            },*/
            validator: validator.isEmail,
            message: `{VALUE} is not a valid email`
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
})

// method felüldefiniálásnak(toJSON) vagy új metódus hozzáadásának(generateAuthToken) standard function-t kell megadni
// felüldefiniáljuk hogy res body-ban ne szerelpeljenoda nem illő adat:  res.header('x-auth', token).send(user)
UserSchema.methods.toJSON = function () {
    var user = this
    var userObject = user.toObject()

    return _.pick(userObject, ['_id', 'email'])
}

UserSchema.methods.generateAuthToken = function () {
    var user = this
    var access = 'auth'
    var token = jwt.sign({_id: user._id.toHexString(), access}, process.env.JWT_SECRET).toString()

    user.tokens.push({access, token})

    return user.save().then(() => {
        return token
    })
}

UserSchema.methods.removeToken = function (token) {
    var user = this

    //mongodb $pull metodus törli a megadottadatot a dokumentumból
    return user.update({
        $pull: {
            tokens: {token}
        }
    })
}

UserSchema.statics.findByToken = function (token) {
    var User = this
    var decoded
    try {
        decoded = jwt.verify(token, process.env.JWT_SECRET)
    } catch (e) {
        return Promise.reject()
    }

    return User.findOne({
        '_id': decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    })
    // return User.findById(decoded._id)
}

UserSchema.statics.findByCredentials = function (email, password) {
    var User = this

    return User.findOne({email}).then((user) => {
        if(!user){
            Promise.reject()
        }
        return new Promise((resolve, reject) => {

            bcrypt.compare(password, user.password, (err, res) => {
                if (res){
                    resolve(user)
                }else{
                    reject()
                }
            })
            
        })
    })
}

//run some code before the given event(save)
UserSchema.pre('save', function (next){
    var user = this

    if (user.isModified('password')){
        bcrypt.genSalt(10, (error, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                 user.password = hash
                 next()
            })
        })
    } else {
        next()
    }
} )

//http://mongoosejs.com/docs/guide.html
var User = mongoose.model('User', UserSchema)
module.exports = {User}