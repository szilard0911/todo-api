var mongoose = require('mongoose')

mongoose.Promise = global.Promise
// mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/TodoApp')
// this uri is set in heroku
mongoose.connect(process.env.MONGODB_URI)

module.exports = {
    mongoose
}

// this var is production in  heroku by default (in locali we dont have so we have to create them we can make 'test' or 'dev' )
//process.env.NODE_ENV === 'production'