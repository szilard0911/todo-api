const {ObjectID} = require('mongodb')
const jwt = require('jsonwebtoken')


const {Todo} = require('./../../models/todo')
const {User} = require('./../../models/user')

const userOneId = new ObjectID()
const userTwoId = new ObjectID()

const users = [{
    _id: userOneId,
    email: 'jen@gmail.com',
    password: 'userPassWord',
    tokens: [{
        access: 'auth',
        token: jwt.sign({_id: userOneId, access: 'auth'}, process.env.JWT_SECRET ).toString()
    }]
}, {
    _id: userTwoId,
    email: 'szil@gmail.com',
    password: 'userPassWord2',
    tokens: [{
        access: 'auth',
        token: jwt.sign({_id: userTwoId, access: 'auth'}, process.env.JWT_SECRET ).toString()
    }]
}]

const todos = [{
    _id: new ObjectID,
    text: 'First test todo',
    _creator: userOneId
}, {
    _id: new ObjectID,
    text: 'Second test todo',
    completed: true,
    completedAt: 123,
    _creator: userTwoId
}]

const populateTodos = (done) => {
    Todo.remove({}).then(() => {
        return Todo.insertMany(todos)
    }).then(() => done())
}
// nem csinálhatjuk insertMany-vel mert akkor nem fut le a password hassing ami a save elé van kötve
const populateUsers = (done) => {
    User.remove({}).then(() => {
        var userOne = new User(users[0]).save()
        var userTwo = new User(users[1]).save()
        // ez valami gecikirály promise
        return Promise.all([userOne, userTwo])
    }).then(() => done())
}

module.exports = {
    todos,
    populateTodos,
    users,
    populateUsers
}